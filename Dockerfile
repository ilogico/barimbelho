FROM ilogico/deno:latest

WORKDIR /usr/barimbelho
COPY server.ts ./
RUN deno cache server.ts
CMD [ "deno", "run", "--allow-net", "server.ts" ]
EXPOSE 8080
