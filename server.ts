import { serve } from 'https://deno.land/std/http/server.ts'

const server = serve({ port: 8080 });

for await (const request of server) {
  request.respond({ body: 'Hello, world!\n' });
}
